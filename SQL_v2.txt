DROP TABLE IF EXISTS "public"."language";

CREATE TABLE "public"."language" (
  "id" serial4,
  "name" varchar(255) NOT NULL,
  PRIMARY KEY ("id")
)
;

INSERT INTO "public"."language" VALUES (1, 'AZE');
INSERT INTO "public"."language" VALUES (2, 'RUS');
INSERT INTO "public"."language" VALUES (3, 'ENG');


---------------------------------------------------------------------------------------

DROP TABLE IF EXISTS "public"."question";

CREATE TABLE "public"."question" (
  "id" serial8,
  "type" varchar(255) NOT NULL,
  "atDelete" date,
  "next_question_id" int8,
  PRIMARY KEY ("id")
)
;

INSERT INTO "public"."question" VALUES (1, 'BUTTON', NULL, 2);
INSERT INTO "public"."question" VALUES (2, 'BUTTON', NULL, 3);
INSERT INTO "public"."question" VALUES (3, 'BUTTON', NULL, 4);
INSERT INTO "public"."question" VALUES (4, 'BUTTON', NULL, 5);
INSERT INTO "public"."question" VALUES (5, 'BUTTON', NULL, 6);
INSERT INTO "public"."question" VALUES (6, 'FREETEXT', NULL, NULL);


---------------------------------------------------------------------------------------

DROP TABLE IF EXISTS "public"."user";

CREATE TABLE "public"."user" (
  "id" serial8,
  "chat_id" serial8,
  "telegram_id" serial8,
  "phone_number" varchar(255) NOT NULL,
  "full_name" varchar(255) NOT NULL,
  PRIMARY KEY ("id")
)
;


---------------------------------------------------------------------------------------

DROP TABLE IF EXISTS "public"."session";

CREATE TABLE "public"."session" (
  "id" uuid NOT NULL,
  "answer_object" json NOT NULL,
  "user_id" serial8,
  "registered_at" timestamp(6) NOT NULL,
  "language_id" serial4,
  PRIMARY KEY ("id"),
  CONSTRAINT "user_id" FOREIGN KEY ("user_id") REFERENCES "public"."user" ("id"),
  CONSTRAINT "language_id" FOREIGN KEY ("language_id") REFERENCES "public"."language" ("id")
)
;


---------------------------------------------------------------------------------------

DROP TABLE IF EXISTS "public"."question_locale";

CREATE TABLE "public"."question_locale" (
  "id" serial8,
  "question" varchar(255) NOT NULL,
  "language_id" serial4,
  "question_id" serial8,
  PRIMARY KEY ("id"),
  CONSTRAINT "language_id" FOREIGN KEY ("language_id") REFERENCES "public"."language" ("id"),
  CONSTRAINT "question_id" FOREIGN KEY ("question_id") REFERENCES "public"."question" ("id")
)
;

INSERT INTO "public"."question_locale" VALUES (1, 'Please, select a language', 3, 1);
INSERT INTO "public"."question_locale" VALUES (2, 'Пожалуйста, выберите язык', 2, 1);
INSERT INTO "public"."question_locale" VALUES (3, 'Zəhmət olmasa dil seçin', 1, 1);

INSERT INTO "public"."question_locale" VALUES (4, 'Choose the type of tour you prefer', 3, 2);
INSERT INTO "public"."question_locale" VALUES (5, 'Выберите тип тура, который вы предпочитаете', 2, 2);
INSERT INTO "public"."question_locale" VALUES (6, 'İstədiyiniz tur növünü seçin', 1, 2);

INSERT INTO "public"."question_locale" VALUES (7, 'What kind of offer do you like to receive?', 3, 3);
INSERT INTO "public"."question_locale" VALUES (8, 'Какое предложение вы хотели бы получить?', 2, 3);
INSERT INTO "public"."question_locale" VALUES (9, 'Necə bir təklif səni maraqlandırır?', 1, 3);

INSERT INTO "public"."question_locale" VALUES (10, 'Domestic or foreign?', 3, 4);
INSERT INTO "public"."question_locale" VALUES (11, 'Внутри страны или за рубежом?', 2, 4);
INSERT INTO "public"."question_locale" VALUES (12, 'Ölkədaxili yoxsa Ölkəxarici?', 1, 4);

INSERT INTO "public"."question_locale" VALUES (13, 'Travel type?', 3, 5);
INSERT INTO "public"."question_locale" VALUES (14, 'Тип путешествия?', 2, 5);
INSERT INTO "public"."question_locale" VALUES (15, 'Səyahət tipi?', 1, 5);

INSERT INTO "public"."question_locale" VALUES (16, 'Where do you want to go?', 3, 6);
INSERT INTO "public"."question_locale" VALUES (17, 'Куда ты хочешь пойти? ', 2, 6);
INSERT INTO "public"."question_locale" VALUES (18, 'Hara getmək istəyirsən?', 1, 6);

INSERT INTO "public"."question_locale" VALUES (19, 'From which city will you leave?', 3, 7);
INSERT INTO "public"."question_locale" VALUES (20, 'Из какого города вы отправитесь?', 2, 7);
INSERT INTO "public"."question_locale" VALUES (21, 'Hansı şəhərdən yola düşəcəksən?', 1, 7);

INSERT INTO "public"."question_locale" VALUES (22, 'Choose your travel start date:', 3, 8);
INSERT INTO "public"."question_locale" VALUES (23, 'Выбор даты начала путешествия:', 2, 8);
INSERT INTO "public"."question_locale" VALUES (24, 'Səyahətin başlanma tarixini seç:', 1, 8);

INSERT INTO "public"."question_locale" VALUES (25, 'Select the end date of the trip', 3, 9);
INSERT INTO "public"."question_locale" VALUES (26, 'Выберите дату окончания поездки', 2, 9);
INSERT INTO "public"."question_locale" VALUES (27, 'Səyahətin bitmə tarixini seç', 1, 9);

INSERT INTO "public"."question_locale" VALUES (28, 'Who-who will you go? When traveling with children, do not forget to mention their age in the amount of USD (eg. "2 Adults , 1 child - 6 years old")', 3, 10);
INSERT INTO "public"."question_locale" VALUES (29, 'Кто-кто пойдет? Когда вы путешествуете с детьми, не забывайте отмечать их возраст (например. "2 взрослых, 1 ребенок-6 лет")', 2, 10);
INSERT INTO "public"."question_locale" VALUES (30, 'Kim-kim gedəcəksiz?Uşaqlarla səyahət etdikdə, onların yaşını qeyd etməyi unutma (məs. "2 böyük, 1 uşaq - 6 yaş")', 1, 10);

INSERT INTO "public"."question_locale" VALUES (31, 'How much budget did you have in mind for travel, huh? (With AZN)', 3, 11);
INSERT INTO "public"."question_locale" VALUES (32, 'Какой бюджет вы предусмотрели для путешествий? (На AZN)', 2, 11);
INSERT INTO "public"."question_locale" VALUES (33, 'Səyahət üçün nə qədər büdcə nəzərdə tutmusan ? (AZN ilə)', 1, 11);

INSERT INTO "public"."question_locale" VALUES (34, 'I received the request. There are no calls from friends who help me, it seems that they are also resting. As soon as I have the opportunity, I will prepare and send suggestions for your request', 3, 12);
INSERT INTO "public"."question_locale" VALUES (35, 'Я получил запрос. Друзьям, которые мне помогают, не звонят, кажется, что они тоже отдыхают.  Как только у меня будет возможность, я подготовлю и отправлю предложения по вашему запросу', 2, 12);
INSERT INTO "public"."question_locale" VALUES (36, 'Sorğunu aldım. Mənə kömək edən dostlara zəng çatmır, deyəsən, onlar da dincəlir.  İmkan tapan kimi istəyinə uyğun təkliflər hazırlayıb göndərəcəm ', 1, 12);


---------------------------------------------------------------------------------------
DROP TABLE IF EXISTS "public"."option";


CREATE TABLE "public"."option" (
  "id" serial8,
  "name" varchar(255) NOT NULL,
  "question_locale_id" serial8,
  PRIMARY KEY ("id"),
  CONSTRAINT "question_locale_id" FOREIGN KEY ("question_locale_id") REFERENCES "public"."question_locale" ("id")
)
;


INSERT INTO "public"."option" VALUES (1, 'English', 1);

INSERT INTO "public"."option" VALUES (1, 'Relax and walking', 1);
INSERT INTO "public"."option" VALUES (2, 'Excursion', 1);
INSERT INTO "public"."option" VALUES (3, 'Active tourism', 1);
INSERT INTO "public"."option" VALUES (4, 'Any type', 1);


INSERT INTO "public"."option" VALUES (5, 'Русский', 2);

INSERT INTO "public"."option" VALUES (5, 'Отдых и прогулки', 2);
INSERT INTO "public"."option" VALUES (6, 'Экскурсионные туры', 2);
INSERT INTO "public"."option" VALUES (7, 'Активный отдых', 2);
INSERT INTO "public"."option" VALUES (8, 'Любой вид отдыха', 2);


INSERT INTO "public"."option" VALUES (9, 'Azərbaycan dili', 3);

INSERT INTO "public"."option" VALUES (9, 'İstirahət-gəzinti', 3);
INSERT INTO "public"."option" VALUES (10, 'Ekskursiya', 3);
INSERT INTO "public"."option" VALUES (11, 'Aktiv istirahət', 3);
INSERT INTO "public"."option" VALUES (12, 'Fərq etməz', 3);
