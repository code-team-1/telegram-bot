/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : PostgreSQL
 Source Server Version : 150001 (150001)
 Source Host           : localhost:5432
 Source Catalog        : TelegramBot2
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 150001 (150001)
 File Encoding         : 65001

 Date: 20/02/2023 22:02:20
*/


-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS "public"."question";
CREATE TABLE "public"."question" (
  "id" int8 NOT NULL DEFAULT nextval('question_id_seq'::regclass),
  "type" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "next_question_id" int8 NOT NULL
)
;

-- ----------------------------
-- Records of question
-- ----------------------------
INSERT INTO "public"."question" VALUES (1, 'BUTTON', 2);
INSERT INTO "public"."question" VALUES (2, 'BUTTON', 3);
INSERT INTO "public"."question" VALUES (3, 'BUTTON', 4);
INSERT INTO "public"."question" VALUES (4, 'BUTTON', 5);
INSERT INTO "public"."question" VALUES (5, 'FREETEXT', -1);

-- ----------------------------
-- Primary Key structure for table question
-- ----------------------------
ALTER TABLE "public"."question" ADD CONSTRAINT "question_pkey" PRIMARY KEY ("id");
