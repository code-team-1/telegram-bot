/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : PostgreSQL
 Source Server Version : 150001 (150001)
 Source Host           : localhost:5432
 Source Catalog        : TelegramBot2
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 150001 (150001)
 File Encoding         : 65001

 Date: 20/02/2023 22:02:13
*/


-- ----------------------------
-- Table structure for option
-- ----------------------------
DROP TABLE IF EXISTS "public"."option";
CREATE TABLE "public"."option" (
  "id" int8 NOT NULL DEFAULT nextval('option_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "question_locale_id" int8 NOT NULL DEFAULT nextval('option_question_locale_id_seq'::regclass)
)
;

-- ----------------------------
-- Records of option
-- ----------------------------
INSERT INTO "public"."option" VALUES (1, 'Relax and walking', 1);
INSERT INTO "public"."option" VALUES (2, 'Excursion', 1);
INSERT INTO "public"."option" VALUES (3, 'Active tourism', 1);
INSERT INTO "public"."option" VALUES (4, 'Any type', 1);
INSERT INTO "public"."option" VALUES (5, 'Отдых и прогулки', 2);
INSERT INTO "public"."option" VALUES (6, 'Экскурсионные туры', 2);
INSERT INTO "public"."option" VALUES (7, 'Активный отдых', 2);
INSERT INTO "public"."option" VALUES (8, 'Любой вид отдыха', 2);
INSERT INTO "public"."option" VALUES (9, 'İstirahət-gəzinti', 3);
INSERT INTO "public"."option" VALUES (10, 'Ekskursiya', 3);
INSERT INTO "public"."option" VALUES (11, 'Aktiv istirahət', 3);
INSERT INTO "public"."option" VALUES (12, 'Fərq etməz', 3);
INSERT INTO "public"."option" VALUES (16, 'Abroad', 7);
INSERT INTO "public"."option" VALUES (17, 'Domestic', 7);
INSERT INTO "public"."option" VALUES (18, 'Group', 10);
INSERT INTO "public"."option" VALUES (19, 'Individual', 10);
INSERT INTO "public"."option" VALUES (14, 'Hotel only', 4);
INSERT INTO "public"."option" VALUES (13, 'Ticket only', 4);
INSERT INTO "public"."option" VALUES (15, 'All inclusive', 4);

-- ----------------------------
-- Primary Key structure for table option
-- ----------------------------
ALTER TABLE "public"."option" ADD CONSTRAINT "option_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table option
-- ----------------------------
ALTER TABLE "public"."option" ADD CONSTRAINT "question_locale_id" FOREIGN KEY ("question_locale_id") REFERENCES "public"."question_locale" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
