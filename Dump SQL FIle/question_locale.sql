/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : PostgreSQL
 Source Server Version : 150001 (150001)
 Source Host           : localhost:5432
 Source Catalog        : TelegramBot2
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 150001 (150001)
 File Encoding         : 65001

 Date: 20/02/2023 22:02:30
*/


-- ----------------------------
-- Table structure for question_locale
-- ----------------------------
DROP TABLE IF EXISTS "public"."question_locale";
CREATE TABLE "public"."question_locale" (
  "id" int8 NOT NULL DEFAULT nextval('question_locale_id_seq'::regclass),
  "question" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "language_id" int4 NOT NULL DEFAULT nextval('question_locale_language_id_seq'::regclass),
  "question_id" int8 NOT NULL DEFAULT nextval('question_locale_question_id_seq'::regclass)
)
;

-- ----------------------------
-- Records of question_locale
-- ----------------------------
INSERT INTO "public"."question_locale" VALUES (1, 'Choose the type of tour you prefer', 3, 1);
INSERT INTO "public"."question_locale" VALUES (2, 'Выберите тип тура, который вы предпочитаете', 2, 1);
INSERT INTO "public"."question_locale" VALUES (3, 'İstədiyiniz tur növünü seçin', 1, 1);
INSERT INTO "public"."question_locale" VALUES (4, 'What kind of offer do you like to receive?', 3, 2);
INSERT INTO "public"."question_locale" VALUES (5, 'Какое предложение вы хотели бы получить?', 2, 2);
INSERT INTO "public"."question_locale" VALUES (6, 'Necə bir təklif səni maraqlandırır?', 1, 2);
INSERT INTO "public"."question_locale" VALUES (8, 'Внутри страны или за рубежом?', 2, 3);
INSERT INTO "public"."question_locale" VALUES (9, 'Ölkədaxili yoxsa Ölkəxarici?', 1, 3);
INSERT INTO "public"."question_locale" VALUES (10, 'Travel type?', 3, 4);
INSERT INTO "public"."question_locale" VALUES (11, 'Тип путешествия?', 2, 4);
INSERT INTO "public"."question_locale" VALUES (12, 'Səyahət tipi?', 1, 4);
INSERT INTO "public"."question_locale" VALUES (13, 'Where do you want to go?', 3, 5);
INSERT INTO "public"."question_locale" VALUES (14, 'Куда ты хочешь пойти? ', 2, 5);
INSERT INTO "public"."question_locale" VALUES (15, 'Hara getmək istəyirsən?', 1, 5);
INSERT INTO "public"."question_locale" VALUES (7, 'Domestic or Abroad?', 3, 3);

-- ----------------------------
-- Primary Key structure for table question_locale
-- ----------------------------
ALTER TABLE "public"."question_locale" ADD CONSTRAINT "question_locale_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table question_locale
-- ----------------------------
ALTER TABLE "public"."question_locale" ADD CONSTRAINT "language_id" FOREIGN KEY ("language_id") REFERENCES "public"."language" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."question_locale" ADD CONSTRAINT "question_id" FOREIGN KEY ("question_id") REFERENCES "public"."question" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
