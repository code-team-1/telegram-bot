/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : PostgreSQL
 Source Server Version : 150001 (150001)
 Source Host           : localhost:5432
 Source Catalog        : TelegramBot2
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 150001 (150001)
 File Encoding         : 65001

 Date: 20/02/2023 22:07:23
*/


-- ----------------------------
-- Table structure for session
-- ----------------------------
DROP TABLE IF EXISTS "public"."session";
CREATE TABLE "public"."session" (
  "id" uuid NOT NULL,
  "answer_object" json NOT NULL,
  "user_id" int8 NOT NULL DEFAULT nextval('session_user_id_seq'::regclass),
  "create_time" timestamp(6) NOT NULL,
  "language_id" int4 NOT NULL DEFAULT nextval('session_language_id_seq'::regclass),
  "chat_id" int8,
  "date" date,
  "user_language" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of session
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table session
-- ----------------------------
ALTER TABLE "public"."session" ADD CONSTRAINT "session_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table session
-- ----------------------------
ALTER TABLE "public"."session" ADD CONSTRAINT "language_id" FOREIGN KEY ("language_id") REFERENCES "public"."language" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."session" ADD CONSTRAINT "user_id" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
