package az.code.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "question")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Question {

    @Id
    long id;

    @Column
    String type;

    @Column
    LocalDateTime atDelete;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "next_question_id", referencedColumnName = "id")
    Question nextQuestion;

    @OneToMany(mappedBy = "question")
    List<QuestionLocale> questionLocales;
}
