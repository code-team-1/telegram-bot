package az.code.entity;


import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Entity
@Table(name = "question_locale")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class QuestionLocale {
    @Id
    long id;

    @Column(name = "question")
    String text;

    @ManyToOne
    @JoinColumn(name = "language_id", referencedColumnName = "id")
    Language language;

    @ManyToOne
    @JoinColumn(name = "question_id", referencedColumnName = "id")
    Question question;

    @OneToMany(mappedBy = "questionLocale")
    List<Option>options;
}
