//package az.code.entity;
//
//import lombok.AccessLevel;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import lombok.experimental.FieldDefaults;
//import org.springframework.data.redis.core.RedisHash;
//
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@RedisHash("Answer")
//@FieldDefaults(level = AccessLevel.PRIVATE)
//public class Answer {
//
//    User user;
//    Language language;
//}
