package az.code.entity;

import az.code.HashMapConverter;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

@Entity
@Table(name = "session")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Session {

    @Id
    UUID id;

    @Convert(converter = HashMapConverter.class)
    Map<String, Object> answer;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    User user;

    @Column(name = "registered_at")
    LocalDateTime registeredAt;

    @ManyToOne
    @JoinColumn(name = "language_id", referencedColumnName = "id")
    Language language;
}
