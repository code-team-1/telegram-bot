package az.code.services;

import az.code.entity.UserData;
import az.code.repository.UserRepository;
import com.vdurmont.emoji.EmojiParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class TelegramBot  {

    @Autowired
    UserRepository userRepository;

    private static Logger logger = LogManager.getLogger(TelegramBot.class);

    private static final String HELP_TEXT = "Write to +9941279283472938";



//    public TelegramBot(BotConfig botConfig) {
//        this.botConfig = botConfig;
//        List<BotCommand> commandList =new ArrayList<>();
//        commandList.add(new BotCommand("/start","Start bot"));
//        commandList.add(new BotCommand("/mydata","Show my data"));
//        commandList.add(new BotCommand("/deletedata","Delete my data"));
//        commandList.add(new BotCommand("/help","Contact with our team"));
//        commandList.add(new BotCommand("/settings","set your preferences"));
//        commandList.add(new BotCommand("/register","Register to app"));
//
//        try {
//            this.execute(new SetMyCommands(commandList,new BotCommandScopeDefault(),null));
//        } catch (TelegramApiException e){
//            logger.error("Error occured :" + e.getMessage());
//        }
//    }
//
//    @Override
//    public String getBotUsername() {
//        return botConfig.getBotName();
//    }
//
//    @Override
//    public String getBotToken() {
//        return botConfig.getToken();
//    }
//
//    @Override
//    public void onUpdateReceived(Update update) {
//
//        if(update.hasMessage()){
//            if(update.getMessage().hasText()){
//                String messageText = update.getMessage().getText();
//                Long chatId = update.getMessage().getChatId();
//                String username = update.getMessage().getChat().getFirstName();
//
//                switch (messageText){
//                    case "/start":
//                        registerUser(update.getMessage());
//                        startCommand(chatId,username);
//                        logger.info("Reply sent to user " + username);
//                        break;
//
//                    case "/help":
//                        sendMessage(chatId,HELP_TEXT);
//                        break;
//
//                    case "/register":
//                        register(chatId);
//                        break;
//                    default:
//                        sendMessage(chatId,"We are developing...");
//                        logger.info("Reply sent to user " + username);
//                }
//            } else if (update.hasCallbackQuery()) {
//                String callBackData = update.getCallbackQuery().getData();
//                long messageId=update.getCallbackQuery().getMessage().getMessageId();
//                long chatId = update.getCallbackQuery().getMessage().getChatId();
//
//                if (callBackData.equals("YES_BUTTON")){
//                    String text = "Yes clicked";
//                    EditMessageText messageText = new EditMessageText();
//                    messageText.setChatId(String.valueOf(chatId));
//                    messageText.setText(text);
//                    messageText.setMessageId((int) messageId);
//
//                    try {
//                        execute(messageText);
//                    } catch (TelegramApiException e) {
//                        logger.error("Error occured :" + e.getMessage());
//                    }
//                } else if (callBackData.equals("NO_BUTTON")) {
//                    String text = "No clicked";
//                    EditMessageText messageText = new EditMessageText();
//                    messageText.setChatId(String.valueOf(chatId));
//                    messageText.setText(text);
//                    messageText.setMessageId((int) messageId);
//
//                    try {
//                        execute(messageText);
//                    } catch (TelegramApiException e) {
//                        logger.error("Error occured :" + e.getMessage());
//                    }
//                }
//
//            }
//        }
//
//    }

    public SendMessage register(Long chatId) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId.toString());
        sendMessage.setText("Register?");

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> rowsLine = new ArrayList<>();
        List<InlineKeyboardButton> keyboardButtonList = new ArrayList<>();

        var yesButton = new InlineKeyboardButton();
        yesButton.setText("yes");
        yesButton.setCallbackData("YES_BUTTON");

        var noButton = new InlineKeyboardButton();
        noButton.setText("no");
        noButton.setCallbackData("NO_BUTTON");

        keyboardButtonList.add(yesButton); keyboardButtonList.add(noButton);
        rowsLine.add(keyboardButtonList);

        inlineKeyboardMarkup.setKeyboard(rowsLine);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);

       return sendMessage;
    }

    public void registerUser(Message msg) {
        if (userRepository.findById(msg.getChatId()).isEmpty()){
            var chatId = msg.getChatId();
            var chat = msg.getChat();

            UserData user = UserData.builder()
                    .id(chatId).name(chat.getFirstName()).surname(chat.getLastName())
                    .username(chat.getUserName()).registeredAt(new Timestamp(System.currentTimeMillis())).build();

            userRepository.save(user);
            log.info("New user registered :" + user.getUsername());
        }
    }

    public SendMessage startCommand(Long chatId,String name){

        String response = EmojiParser.parseToUnicode("Hi " + name + " wellcome to our bot!" + ":goat:");

        return sendMessage(chatId,response);

    }

    public SendMessage sendMessage(Long chatId,String msg){
        SendMessage message = new SendMessage();
        message.setChatId(chatId.toString());
        message.setText(msg);

        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();

        List<KeyboardRow> keyboardRows = new ArrayList<>();

        KeyboardRow row = new KeyboardRow();

        row.add("Time");  row.add("Weather"); keyboardRows.add(row);

        row = new KeyboardRow(); row.add("register"); row.add("check my data"); row.add("delete my data");
        keyboardRows.add(row);

        keyboardMarkup.setKeyboard(keyboardRows);

        message.setReplyMarkup(keyboardMarkup);

       return message;

    }

}