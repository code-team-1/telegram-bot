package az.code.repository;

import az.code.entity.Language;
import az.code.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question,Long> {

    Question findQuestionsByLanguage(Language language);
}
