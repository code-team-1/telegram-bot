//package com.example.controller;
//
//import com.example.config.BotConfig;
//import com.example.services.TelegramBot;
//import com.example.statics.StaticTexts;
//import lombok.RequiredArgsConstructor;
//import lombok.SneakyThrows;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.telegram.telegrambots.bots.TelegramLongPollingBot;
//import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
//import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
//import org.telegram.telegrambots.meta.api.objects.Update;
//import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
//import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeDefault;
//import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static com.example.statics.StaticTexts.HELP_TEXT;
//
//@Component
//
//@Slf4j
//public class CommandController extends TelegramLongPollingBot {
//
//    @Autowired
//    TelegramBot telegramBot;
//
//    private final BotConfig botConfig;
//    @Override
//    public String getBotUsername() {
//        return botConfig.getBotName();
//    }
//
//    @Override
//    public String getBotToken() {
//        return botConfig.getToken();
//    }
//
//    @SneakyThrows
//    @Override
//    public void onUpdateReceived(Update update) {
//        if(update.hasMessage()){
//            if(update.getMessage().hasText()){
//                String messageText = update.getMessage().getText();
//                Long chatId = update.getMessage().getChatId();
//                String username = update.getMessage().getChat().getFirstName();
//
//                switch (messageText) {
//                    case "/start" -> {
//                        telegramBot.registerUser(update.getMessage());
//
//                            execute(telegramBot.startCommand(chatId, username));
//
//                        log.info("Reply sent to user " + username);
//                    }
//                    case "/help" -> execute(telegramBot.sendMessage(chatId, HELP_TEXT));
//                    case "/register" -> {
//                        telegramBot.register(chatId);
//                        execute(telegramBot.register(chatId));
//                        String callBackData = update.getCallbackQuery().getData();
//                        long messageId=update.getCallbackQuery().getMessage().getMessageId();
//                        if (callBackData.equals("YES_BUTTON")){
//                            String text = "Yes clicked";
//                            EditMessageText messageText1 = new EditMessageText();
//                            messageText1.setChatId(String.valueOf(chatId));
//                            messageText1.setText(text);
//                            messageText1.setMessageId((int) messageId);
//
//
//                                execute(messageText1);
//                        } else if (callBackData.equals("NO_BUTTON")) {
//                            String text = "No clicked";
//                            EditMessageText messageText1 = new EditMessageText();
//                            messageText1.setChatId(String.valueOf(chatId));
//                            messageText1.setText(text);
//                            messageText1.setMessageId((int) messageId);
//
//                            try {
//                                execute(messageText1);
//                            } catch (TelegramApiException e) {
//                                log.error("Error occured :" + e.getMessage());
//                            }
//                        }
//
//                    }
//                    default -> {
//                        telegramBot.sendMessage(chatId, "We are developing...");
//
//                        execute(telegramBot.sendMessage(chatId, "We are developing..."));
//
//                        log.info("Reply sent to user " + username);
//                    }
//                }
//            } else if (update.hasCallbackQuery()) {
//                String callBackData = update.getCallbackQuery().getData();
//                long messageId=update.getCallbackQuery().getMessage().getMessageId();
//                long chatId = update.getCallbackQuery().getMessage().getChatId();
//
//                if (callBackData.equals("YES_BUTTON")){
//                    String text = "Yes clicked";
//                    EditMessageText messageText = new EditMessageText();
//                    messageText.setChatId(String.valueOf(chatId));
//                    messageText.setText(text);
//                    messageText.setMessageId((int) messageId);
//
//                    try {
//                        execute(messageText);
//                    } catch (TelegramApiException e) {
//                        log.error("Error occured :" + e.getMessage());
//                    }
//                } else if (callBackData.equals("NO_BUTTON")) {
//                    String text = "No clicked";
//                    EditMessageText messageText = new EditMessageText();
//                    messageText.setChatId(String.valueOf(chatId));
//                    messageText.setText(text);
//                    messageText.setMessageId((int) messageId);
//
//                    try {
//                        execute(messageText);
//                    } catch (TelegramApiException e) {
//                        log.error("Error occured :" + e.getMessage());
//                    }
//                }
//
//            }
//        }
//
//    }
//
//
//    public CommandController(BotConfig botConfig) {
//        this.botConfig = botConfig;
//        List<BotCommand> commandList =new ArrayList<>();
//        commandList.add(new BotCommand("/start","Start bot"));
//        commandList.add(new BotCommand("/mydata","Show my data"));
//        commandList.add(new BotCommand("/deletedata","Delete my data"));
//        commandList.add(new BotCommand("/help","Contact with our team"));
//        commandList.add(new BotCommand("/settings","set your preferences"));
//        commandList.add(new BotCommand("/register","Register to app"));
//
//        try {
//            this.execute(new SetMyCommands(commandList,new BotCommandScopeDefault(),null));
//        } catch (TelegramApiException e){
//            log.error("Error occured :" + e.getMessage());
//        }
//    }
//}
