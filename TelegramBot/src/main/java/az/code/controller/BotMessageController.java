//package az.code.controller;
//
//import com.example.telegramtourbot.configuration.BotConfig;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.telegram.telegrambots.bots.TelegramWebhookBot;
//import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
//import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
//import org.telegram.telegrambots.meta.api.objects.Update;
//import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
//
//@Component
//public class BotMessageController extends TelegramWebhookBot {
//
//    @Autowired
//    BotConfig botConfig;
//
//    @Override
//    public String getBotUsername() {
//        return botConfig.getBotName();
//    }
//
//    @Override
//    public String getBotToken() {
//        return botConfig.getToken();
//    }
//
//    @Override
//    public BotApiMethod<?> onWebhookUpdateReceived(Update update) {
//        if (update.getMessage() != null && update.getMessage().hasText()) {
//            String chat_id = update.getMessage().getChatId().toString();
//
//
//            try {
//                execute(new SendMessage(chat_id, "Hi " + update.getMessage().getText()));
//            } catch (TelegramApiException e) {
//                e.printStackTrace();
//            }
//        }
//
//        return null;
//    }
//
//    @Override
//    public String getBotPath() {
//        return botConfig.getPath();
//    }
//}
