package az.code.telegrambotv2.repositories;

//import az.code.telegrambotv2.SessionHistoryDto;
import az.code.telegrambotv2.models.entities.SessionsHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


public interface HistoryRepository extends JpaRepository<SessionsHistory, UUID> {
}
