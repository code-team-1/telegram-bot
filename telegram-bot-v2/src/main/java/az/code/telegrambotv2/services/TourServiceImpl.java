package az.code.telegrambotv2.services;

//import az.code.telegrambotv2.SessionHistoryDto;
//import az.code.telegrambotv2.repositories.HistoryRepository;

import az.code.telegrambotv2.bot.TourBot;
import az.code.telegrambotv2.models.entities.*;
import az.code.telegrambotv2.repositories.OptionRepository;
import az.code.telegrambotv2.repositories.redis.SessionRepository;
import az.code.telegrambotv2.services.interfaces.*;
import az.code.telegrambotv2.utils.Messages;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;


@Component
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Slf4j
public class TourServiceImpl implements TourService {

    TourBot tourBot;
    LanguageService languageService;

    ProcessService processService;

    QuestionServiceImpl questionService;
    QuestionLocaleService questionLocaleService;

    OptionRepository optionRepository;

//    @Autowired
//    HistoryRepository historyRepository;

//    @Autowired
//    SessionRepository sessionRepository;


    public TourServiceImpl(@Lazy TourBot tourBot, LanguageService languageService, ProcessService processService,
                           OptionRepository optionRepository, QuestionLocaleService questionLocaleService,
                           QuestionServiceImpl questionService) {

        this.tourBot = tourBot;
        this.languageService = languageService;
        this.processService = processService;
        this.optionRepository = optionRepository;
        this.questionLocaleService = questionLocaleService;
        this.questionService = questionService;
    }


    @Override
    public BotApiMethod<?> handleUpdate(Update update) throws TelegramApiException {
        log.error("----------------------------------------------------------------------");

        log.error("1");
        if (!validateUpdate(update)) {
            log.error(" 1 ???");
            return new SendMessage(update.getMessage().getChatId().toString(), Messages.incorrectCommandMessage());
        }

        log.error("2");

        Message message = update.getMessage();

        log.error("3");
        // handle commands
        if (message.getText().startsWith("/")) {
            return handleCommands(message);
        }

        log.error("4");
        // handle message before session activation
        if (!processService.hasActiveSession(Long.parseLong(message.getChatId().toString()))) {
            return new SendMessage(message.getChatId().toString(), Messages.sessionMessage());
        }

        log.error("5");

        try {
            if (!isOption(update))
                return new SendMessage(update.getMessage().getChatId().toString(), Messages.incorrectCommandMessage());
        } catch (IllegalArgumentException e) {
            long id = processService.getCurrentQuestionId(message.getChatId());
            Question question = questionService.findById(id);

            if (question.getNextQuestionId() == -1) {
                return new SendMessage(Long.toString(id), Messages.existRequestMessage());
            } else {
                return new SendMessage(Long.toString(id), Messages.incorrectDateMessage());
            }
        }


        log.error("6");
//        if (message.isReply()){
//            return handleReplyMessage(message);
//        }

        log.error("7");
        return handleMessage(message);
    }

    private BotApiMethod<?> handleCommands(Message message) {
        String chatId = message.getChatId().toString();
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);

        switch (message.getText()) {
            case "/start":
                if (processService.hasActiveSession(Long.parseLong(chatId))) {
                    return new SendMessage(chatId, Messages.activeSessionMessage());
                }
                //TODO register user
                processService.createSession(message);
                return giveLanguageChoice(chatId);
            case "/stop":
                if (!processService.hasActiveSession(Long.parseLong(chatId))) {
                    return new SendMessage(chatId, Messages.sessionMessage());
                }
                sendMessage.setReplyMarkup(new ReplyKeyboardRemove(true));
                sendMessage.setText(Messages.stopMessage());
                // stop session
                processService.stopActivePoll(Long.parseLong(chatId));
                break;
            default:
                sendMessage.setText(Messages.incorrectCommandMessage());
                break;
        }
        return sendMessage;
    }

    private BotApiMethod<?> handleMessage(Message message){

        long chatId = message.getChatId();
        String answer = message.getText();

        long currentQuestionId = processService.getCurrentQuestionId(chatId);
        Question question = questionService.findById(currentQuestionId);

        log.error("currentQuestionId " + currentQuestionId);

        // set language
        if (currentQuestionId == 0) {

            processService.setSelectedLanguage(chatId, answer);
            // the language has been selected, go to the first question
            question = questionService.findById(1);
            processService.setCurrentQuestionId(chatId, 1);

            return giveQuestion(chatId, question);
        }

        if (question.getType().equals("DATE")) {
            //TODO date
            addUserAnswer(chatId, question, answer);
        }

        // last question
        if (question.getNextQuestionId() == -1) {
            log.error("last question " + question.getNextQuestionId());
            // take the text of the question and save in Redis
            addUserAnswer(chatId, question, answer);


//            historyRepository.save(new SessionHistoryDto(sessionRepository.find(chatId)));

            // TODO close session?
//                processService.stopActivePoll(Long.parseLong(chatId));
            // finish message
            return new SendMessage(String.valueOf(chatId), Messages.successRequestMessage());
        }


        // questions from 1 to n-1
        if (currentQuestionId > 0) {
            // take the text of the question and save in Redis
            addUserAnswer(chatId, question, answer);
            // update current question in Redis
            long id = question.getNextQuestionId();
            question = questionService.findById(id);
            processService.setCurrentQuestionId(chatId, id);

            return giveQuestion(chatId, question);
        }

        return null;


//        if (questionBag.isDate(question)) return new SendMessage(chatId, incorrectAnswer(service.getSelectedLanguage(clientId)));
//        if (!validateQuestion(question, answer)){
//            return giveQuestion(chatId, clientId, question, incorrectAnswer(service.getSelectedLanguage(clientId)));
//        }
//        if (questionBag.isFirst(question)) service.setSelectedLanguage(clientId, answer);
//        service.saveUserData(clientId, question.getQuestionKey(), answer);
//        return giveQuestion(chatId, clientId, questionBag.getNext(question, answer), null);
    }

    @SneakyThrows
    private BotApiMethod<?> giveLanguageChoice(String chatId){

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(Messages.greetingMessage());

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboardRowList = new ArrayList<>();

        KeyboardRow row = new KeyboardRow();

        languageService.getAll().forEach(language -> {
            KeyboardButton button = new KeyboardButton();
            button.setText(language.getName());
            row.add(button);
        });

        keyboardRowList.add(row);

        replyKeyboardMarkup.setKeyboard(keyboardRowList);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        replyKeyboardMarkup.setResizeKeyboard(true);
        sendMessage.setReplyMarkup(replyKeyboardMarkup);

        return sendMessage;
    }

    @SneakyThrows
    private BotApiMethod<?> giveQuestion(long chatId, Question question){

        QuestionLocale currentQuestionLocale = getLocaleOfQuestion(chatId, question);

        // set text of question
        SendMessage sendMessage = new SendMessage(Long.toString(chatId), currentQuestionLocale.getText());
        // set options of question
        sendMessage.setReplyMarkup(stageButton(chatId, question));
        // remove buttons
        if (question.getType().equals("FREETEXT")) sendMessage.setReplyMarkup(new ReplyKeyboardRemove(true));

////        if (questionBag.isDate(question))sendMessage.setReplyMarkup(generateKeyboard(LocalDate.now()));
////        if (questionBag.hasButton(question))sendMessage.setReplyMarkup(getButtons(clientId, question));
////        if (questionBag.isLast(question)) service.endPoll(clientId);
////        if (questionBag.isEnding(question)) return service.sendSelection(clientId);
//
//        sendMessage.setReplyMarkup(stageButton(lang, question.getId()));

        return sendMessage;
    }

    public ReplyKeyboardMarkup stageButton(long chatId, Question question) {

        List<Option> optionList = getLocaleOfQuestion(chatId, question).getOptions();

        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboardRowList = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();

        for (Option option : optionList) {
            KeyboardButton button = new KeyboardButton();
            button.setText(option.getAnswer());
            row.add(button);
        }

        keyboardRowList.add(row);

        keyboardMarkup.setKeyboard(keyboardRowList);
        keyboardMarkup.setSelective(true);
        keyboardMarkup.setResizeKeyboard(true);
        keyboardMarkup.setOneTimeKeyboard(false);
        return keyboardMarkup;
    }

    @SneakyThrows
    public Boolean validateUpdate(Update update){
        Long clientId = null;

        // TODO handle emoji
        if (update.getMessage().getText() == null){
            log.error("update.getMessage().getText()");
            return false;
        }

        if (update.hasCallbackQuery()){

            log.error("1.1" + update.getCallbackQuery().toString());
            clientId = update.getCallbackQuery().getFrom().getId();
        }  else if (update.hasMessage()) {
            log.error("1.2" + update.getMessage().getText());
            if (!update.getMessage().hasText()) return false;
            log.error("1.3" + update.getMessage().getText());
            if (update.getMessage().getText().startsWith("/")) return true;
            clientId = update.getMessage().getFrom().getId();
        }else {
            log.error("1.4");
            return false;
        }
        if (!processService.hasActiveSession(clientId)){
            log.error("1.5");
            log.error("!processService.hasActiveSession(clientId) " + update.getMessage().getText() + " " + clientId);
            tourBot.execute(new SendMessage(clientId.toString(), Messages.sessionMessage()));
            return false;
        }
        log.error("1.6");
        return true;
    }
    @SneakyThrows
    public Boolean isOption(Update update) {

        String messageText = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();
        long currentQuestionId = processService.getCurrentQuestionId(chatId);

        log.error("IS OPTION currentQuestionId " + currentQuestionId);
        Question currentQuestion = questionService.findById(currentQuestionId);

        log.error("currentQuestionId " + currentQuestionId);
        log.error("currentQuestion " + currentQuestion);
        // language check
        if (currentQuestionId == 0) {

            List<Language> languages = languageService.getAll();

            for (Language l : languages) {
                if (l.getName().equals(messageText)) {
                    return true;
                }
            }
            return false;
        } else {

            QuestionLocale currentQuestionLocale = getLocaleOfQuestion(chatId, currentQuestion);

            log.error("language check else " +  currentQuestionId);
            log.error("language check question " +  currentQuestion);
            log.error("language check question " +  currentQuestionLocale);
            log.error("messageText " + messageText);

            // FreeText
            if (currentQuestion.getType().equals("FREETEXT")) return true;

            // Date
            if (currentQuestion.getType().equals("DATE")) {

                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
                DateTime dt = formatter.parseDateTime(messageText);

                log.error("TIME " + dt);
                log.error("TIME " +dt.minusDays(1) +" "+ dt.isAfter(dt.minusDays(1)));
                log.error("TIME " + dt.plusYears(1)+ " " + dt.isBefore(dt.plusYears(1)));

                //TODO from  2024-02-10 to 2023-02-10
                //TODO validation text input
                if (dt.isAfter(DateTime.now().minusDays(1)) && dt.isBefore(DateTime.now().plusYears(1))) {
                    return true;
                } else {
                    log.error("BOOM!");
                    throw new IllegalFieldValueException(DateTimeFieldType.year(),0,null,null);
                }
            }

            // Button
            for (Option x : currentQuestionLocale.getOptions()) {
                if (x.getAnswer().equals(messageText)) {
                    log.error("x.getAnswer() " + x.getAnswer());
                    return true;
                }
            }
        }

        log.error("POSLE POSLE");
        return false;
    }

    //TODO  is it possible to replace req with a query?
    public QuestionLocale getLocaleOfQuestion(long chatId, Question currentQuestion) {

        List<QuestionLocale> questionLocaleList = currentQuestion.getTextOfQuestion();
        String lang = processService.getSelectedLanguage(chatId);

        QuestionLocale currentQuestionLocale = null;
        for (QuestionLocale x : questionLocaleList) {
            if (x.getLanguage().getName().equals(lang)) {
                currentQuestionLocale = x;
            }
        }
        return currentQuestionLocale;
    }

    public void addUserAnswer(long chatId, Question question, String answer) {
        QuestionLocale questionLocale = getLocaleOfQuestion(chatId, question);
        processService.saveUserAnswers(chatId, questionLocale.getText(), answer);
    }


}
