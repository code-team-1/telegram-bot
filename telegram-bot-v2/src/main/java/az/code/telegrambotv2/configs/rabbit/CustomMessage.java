package az.code.telegrambotv2.configs.rabbit;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CustomMessage {

    String messageId;

    String message;

    Date messageDate;
}

