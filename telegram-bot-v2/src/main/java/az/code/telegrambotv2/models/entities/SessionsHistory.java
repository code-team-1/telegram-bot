package az.code.telegrambotv2.models.entities;


import az.code.telegrambotv2.JpaConverterJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Map;
import java.util.UUID;

@Entity
@Table(name = "session")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SessionsHistory {

    @Id
    UUID id;

    Long chatId;
    @Convert(converter = JpaConverterJson.class)
    Map<String, String> answers;
//    Question currentQuestion;
    String userLanguage;
    Timestamp date;

}
