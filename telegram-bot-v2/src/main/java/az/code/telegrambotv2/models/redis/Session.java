package az.code.telegrambotv2.models.redis;

import az.code.telegrambotv2.models.entities.Question;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serial;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Session implements Serializable {
    @Serial
    private static final long serialVersionUID = -1691935946363316490L;
    String uuid;
    Long chatId;
    Map<String, String> answers;

    //TODO long id
//    Question currentQuestion;
    long currentQuestionId;

    String userLanguage;
    Timestamp date;
}
