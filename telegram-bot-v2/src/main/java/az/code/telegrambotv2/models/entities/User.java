package az.code.telegrambotv2.models.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "users")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User {

    @Id
    long id;

    @Column(name = "chat_id")
    long chatId;

    @Column(name = "name")
    String name;

    @Column(name = "surname")
    String surname;

    @Column(name = "username")
    String username;

    @Column(name = "registered_at")
    Timestamp registeredAt;

    @Column(name = "phone_number")
    String phoneNumber;
}
