//package az.code.telegrambotv2;
//
//import az.code.telegrambotv2.models.redis.Session;
//import jakarta.persistence.Id;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import java.io.Serializable;
//import java.sql.Timestamp;
//import java.util.Map;
//import java.util.UUID;
//
//@Data
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
//public class SessionHistoryDto implements Serializable {
//
//    private Map<String, String> answers;
//
//    public SessionHistoryDto(Session session) {
//        this.answers = session.getAnswers();
//    }
//}
